package com.test.statictest;

import android.app.Activity;
import android.os.Bundle;


public class MainActivity extends Activity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        doTest();
    }

    public static void main(String[] args) {
        doTest();
    }

    private static void doTest() {
        System.out.println(StaticObject.TEST_TEXT);
        System.out.println("Now we will get singleton");
        StaticObject so = StaticObject.staticObject;
    }
}


