package com.test.statictest;

/**
 * Created by niksl_000 on 12.05.2014.
 */
public class StaticObject {

    public static StaticObject staticObject = new StaticObject();
    public static final String TEST_TEXT = "Hello World";

    public StaticObject() {
        System.out.println("Static object created");
    }
}
